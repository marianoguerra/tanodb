-module(tanodb_write_fsm).
-behavior(gen_fsm).

%% API
-export([start_link/7, run/7]).

%% Callbacks
-export([init/1, code_change/4, handle_event/3, handle_info/3,
         handle_sync_event/4, terminate/3]).

-ignore_xref([start_link/7, init/1, code_change/4, handle_event/3,
              handle_info/3, handle_sync_event/4, terminate/3, run/7]).

-export([prepare/2, execute/2, waiting/2]).

-ignore_xref([prepare/2, execute/2, waiting/2]).

%% req_id: The request id so the caller can verify the response.
%% sender: The pid of the sender so a reply can be made.
%% prelist: The preflist for the given {Bucket, Key} pair.
%% num_w: The number of successful replies.
-record(state, {req_id :: pos_integer(),
                from :: pid(),
                n :: pos_integer(),
                w :: pos_integer(),
                key :: term(),
                params :: term(),
                accum = [],
                action :: atom(),
                preflist :: riak_core_apl:preflist2(),
                num_w = 0 :: non_neg_integer()}).

%%%===================================================================
%%% API
%%%===================================================================

start_link(ReqId, From, Key, Params, N, W, Action) ->
    gen_fsm:start_link(?MODULE, [ReqId, From, Key, Params, N, W, Action],
                       []).

run(Action, Key, Params, N, W, Pid, ReqId) ->
    tanodb_write_fsm_sup:start_fsm([ReqId, Pid, Key, Params, N, W, Action]),
    {ok, ReqId}.

%%%===================================================================
%%% States
%%%===================================================================

%% @doc Initialize the state data.
init([ReqId, From, Key, Params, N, W, Action]) ->
    SD = #state{req_id=ReqId, from=From, action=Action, key=Key, params=Params,
                n=N, w=W},
    {ok, prepare, SD, 0}.

%% @doc Prepare by calculating the _preference list_.
prepare(timeout, SD0=#state{n=N, key=Key}) ->
    DocIdx = riak_core_util:chash_key(Key),
    Preflist = riak_core_apl:get_apl(DocIdx, N, tanodb),
    SD = SD0#state{preflist=Preflist},
    {next_state, execute, SD, 0}.

%% @doc Execute the request and then go into waiting state to
%% verify it has meets consistency requirements.
execute(timeout, SD0=#state{req_id=ReqId, action=Action, params=Params,
                            preflist=Preflist}) ->
    Command = {Action, ReqId, Params},
    riak_core_vnode_master:command(Preflist, Command, {fsm, undefined, self()},
                                   tanodb_vnode_master),
    {next_state, waiting, SD0}.

%% @doc Wait for W reqs to respond.
waiting({ReqId, Result}, SD0=#state{from=From, num_w=NumW0, w=W, accum=Accum}) ->
    NumW = NumW0 + 1,
    Accum1 = [Result|Accum],
    SD = SD0#state{num_w=NumW, accum=Accum1},
    if NumW =:= W ->
           From ! {ReqId, {ok, Accum1}},
           {stop, normal, SD};
       true -> {next_state, waiting, SD}
    end.

handle_info(Info, _StateName, StateData) ->
    lager:warning("got unexpected info ~p", [Info]),
    {stop, badmsg, StateData}.

handle_event(Event, _StateName, StateData) ->
    lager:warning("got unexpected event ~p", [Event]),
    {stop, badmsg, StateData}.

handle_sync_event(Event, _From, _StateName, StateData) ->
    lager:warning("got unexpected sync event ~p", [Event]),
    {stop, badmsg, StateData}.

code_change(_OldVsn, StateName, State, _Extra) -> {ok, StateName, State}.
terminate(_Reason, _SN, _SD) -> ok.

